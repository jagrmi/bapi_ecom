package config

const (
	VERSION = "0.1.3"
	BUILD   = "dev"
)

var BuildVersion string

func GetBuildVersion() string {
	if BuildVersion != "" {
		return BuildVersion
	}
	return BUILD
}

func GetVersion() string {
	return VERSION
}
