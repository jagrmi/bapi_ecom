package config

import (
	"github.com/rs/zerolog"
)

type Config struct {
	APIPort         	string
}

type Db struct {
	ReconnectTime int
	ConnectTimeout int
	Host string
	Port int
	User string
	Password string
	PasswordEncryption string
	PasswordEncryptionKeyFromFile string
	DBName string
}

type Logging struct {
	ConsoleLoggingEnabled bool
	EncodeLogsAsJson bool
	FileLoggingEnabled bool
	Directory string
	Filename string
	FilenameHttp string
	MaxSize int
	MaxBackups int
	MaxAge int
	LogLevel zerolog.Level
}

type Gates struct {
	ConnectTimeout int
}

type Redis struct {
	Url string
	ReconnectTime int
	ConnectAttempt int
}
