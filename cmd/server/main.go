package main

import (
	"flag"
	"fmt"
	"log"
	"github.com/spf13/viper"
	"gitlab.com/jagrmi/bapi_ecom/config"
	// "vc2.devpc.ru/ivan.solonitsyn/tkb-go-app/config"
)

func processFlags() {
	var pkg, build string
	flag.StringVar(&pkg, "buildpkg", "", "Building Packages")
	// flag.StringVar(&build, "buildver", config.GetBuildVersion(), "Build version")
	flag.Parse()
	if pkg == "rpm" {
		fmt.Println(build)
		// log.Info().Msg("Create RPM package")
		// if setup.BuildPKG(pkg, build) {
		// 	log.Info().Msg("Building RPM - yes!")
		// } else {
		// 	log.Info().Msg("Building RPM - failed")
		// }
		// os.Exit(0)
	}
}

func init() {
	viper.SetConfigName(".config.default")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./configs/")
}

func main() {
	processFlags()
	fmt.Println("Hi")

	if err := viper.ReadInConfig(); err != nil {
		// log.Fatal().Err(err).Msg("Error reading default config")
		log.Fatal("Error reading default config")
	}
	viper.SetConfigName("config")
	viper.AddConfigPath("./configs/")
	if err := viper.MergeInConfig(); err != nil {
		// log.Fatal().Err(err).Msg("Error reading config")
		log.Fatal("Error reading config")
	}

	var cfg config.Config
	if err := viper.Sub("config").Unmarshal(&cfg); err != nil {
		// log.Fatal().Err(err).Msg("Error parsing config")
		log.Fatal("Error reading config")
	}
}
